import { createRouter, createWebHashHistory } from 'vue-router';

import useTheme from '@/composables/useTheme';

import EventView from '@/views/EventView.vue';
import EventsView from '@/views/EventsView.vue';

const theme = useTheme();

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),

  routes: [
    {
      path: '/',
      name: 'events',
      component: EventsView,
    },
    {
      path: '/event/:id',
      name: 'event',
      component: EventView,
    },
  ],

  scrollBehavior(to, from, savedPosition) {
    // Ignore navigation caused just by query change
    if (
      to.name === from.name &&
      JSON.stringify(to.params) === JSON.stringify(from.params)
    ) {
      return false;
    }

    if (savedPosition) {
      return savedPosition;
    }

    /**
     * The web component is not necessarily embedded at the top of the page
     * When the user navigates, we want to move the scroll position to be
     * close to the top of it.
     */
    // Enable this behavior only when the user is navigating within the component
    if (from.matched.length) {
      const agendaWrapper = document.querySelector('agenda-embed');

      if (agendaWrapper) {
        const agendaOffsetTop =
          agendaWrapper.getBoundingClientRect().top + window.scrollY;

        return {
          top: Math.max(0, agendaOffsetTop - theme.getScrollOffsetHeight()),
        };
      }
    }

    return {
      top: 0,
    };
  },
});

export default router;
