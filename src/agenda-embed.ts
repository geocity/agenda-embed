import { defineCustomElement } from 'vue';

import AgendaEmbed from './AgendaEmbed.ce.vue';

import '@/mocks';

customElements.define('agenda-embed', defineCustomElement(AgendaEmbed));
