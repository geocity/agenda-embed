import { defineCustomElement } from 'vue';

import AgendaUpcomingEmbed from './AgendaUpcomingEmbed.ce.vue';

import '@/mocks';

customElements.define(
  'agenda-upcoming-embed',
  defineCustomElement(AgendaUpcomingEmbed)
);
