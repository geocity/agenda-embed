import { type FunctionalComponent, h } from 'vue';

const Fragment: FunctionalComponent = (props, { slots }) => {
  return h(() => slots.default?.());
};

export default Fragment;
