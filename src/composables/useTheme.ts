import { ref } from 'vue';

export interface ThemeConfig {
  scrollOffsets?: [number, number][];
}

const config = ref<ThemeConfig>({});

/**
 * Save the scroll offset config pre-ordered with the biggest breakpoint first
 */
function setScrollOffsets(scrollOffsets: ThemeConfig['scrollOffsets']) {
  if (scrollOffsets?.length) {
    config.value.scrollOffsets = scrollOffsets.sort((a, b) => b[0] - a[0]);
  }
}

/**
 * Return the most appropriate offset based on the current viewport width
 */
function getScrollOffsetHeight() {
  if (!config.value.scrollOffsets) {
    return 0;
  }

  let offsetHeight = 0;
  const viewportWidth = window.innerWidth;

  for (const [key, value] of config.value.scrollOffsets) {
    if (viewportWidth >= key) {
      offsetHeight = value;
      break;
    }
  }

  return offsetHeight;
}

export default function useTheme() {
  return {
    config,
    setScrollOffsets,
    getScrollOffsetHeight,
  };
}
