# Contributing

## Project Setup

The currently required Node.js version is specified in the `.nvmrc` file.

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm start
```

You can browse the web component in various context:

- Standalone: [localhost:3000](http://localhost:3000/)
- Yverdon-les-Bains website: [localhost:3000/yverdon.html](http://localhost:3000/yverdon.html)
- Cultyv website: [localhost:3000/cultyv.html](http://localhost:3000/cultyv.html)

### Type-Check, Compile and Minify for Production

The following command will create files in the `dist` folder.

```sh
npm run build
```

### Lint/Format with ESLint & Prettier

```sh
# Check code styling
npm run validate
# Fix as many offenses as possible automatically
npm run format
```
