function accordeon() {
    $(".panel-heading").click(function () {

        if ($(this).parent().hasClass("open")) {
            $(this).parent().removeClass("open");
            $(this).parent().siblings().removeClass("open");
            $(this).addClass("show");        }
        else {
            $(".panel .panel-heading > div").attr("aria-expanded", "false");
            $(this).addClass("collapsed");
            $(this).removeClass("show");
            $(this).parent().siblings().removeClass("open");
            $(this).parent().addClass("open");

            setTimeout(
                function () {
                    var speed = 400;
                    var hrefTarget = $(this).parent();
                    var menuHeight = $("#header .navbar").height() + 20;
                    $("html, body").animate(
                        {
                            scrollTop: $(hrefTarget).offset().top - menuHeight,
                        },
                        speed
                    );
                }.bind(this),
                350
            );
        }

        if ($(this).find("tab-carousel")) {
            $(".tab-carousel").resize();
            $(".tab-content").resize();
        }
    });
    $('.panel-collapse').each(function(){
        $(this).attr('data-parent', '#'+ $(this).closest('.panel-group').attr('id'));
    })
}

function accordeonOpenFromURL() {
    if (window.location.hash) {
        if (window.location.href.indexOf("#/") < 1) {
            var hash = window.location.hash.substring(1);
            $("#" + hash)
                .parents(".panel-collapse")
                .addClass("show");
            console.log(hash);
            //$('#'+hash).parents('.panel.panel-default').find('.panel-heading > div').removeClass('collapsed');
            $("#" + hash)
                .parent()
                .parent()
                .parent(".panel.panel-default")
                .find(".panel-heading > div")
                .removeClass("collapsed");
            $("#" + hash)
                .parents(".panel-collapse")
                .find(".panel-heading")
                .first()
                .attr("aria-expanded", "true");
            $("#" + hash)
                .parents(".panel.panel-default")
                .addClass("open");

            // scroll to accordion
            var hrefTarget = $("#" + hash).closest(".panel-default");
            var menuHeight = $("#header").height();
            $("html, body").animate(
                {
                    scrollTop: $(hrefTarget).offset().top - menuHeight - 20,
                },
                300
            );
        }
    }
}

function carouselTab() {
    $(".tab-carousel").each(function (index) {
        $(this).addClass("num-" + index);

        $(".tab-carousel.num-" + index).slick({
            slidesToShow: 1,
            focusOnSelect: true,
            arrows: false,
            speed: 500,
            asNavFor: ".num-" + index,
            variableWidth: true,
        });
    });

    $(".tab-content").each(function (index) {
        $(this).addClass("num-" + index);

        $(".tab-content.num-" + index).slick({
            slidesToShow: 1,
            arrows: false,
            asNavFor: ".num-" + index,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        dots: true,
                    },
                },
            ],
        });
    });
}

//INPUT LABEL ANIMATION
function formsAnimation() {
    $('.powermail_fieldwrap_type_input input, .powermail_fieldwrap_type_textarea textarea, .form-input:not(.form-input-checkbox) input, .form-input textarea, .input-container input, .input-container textarea').focus(function() {
        $(this).parent().addClass('active');
        $(this).parent().addClass('complete');
    });

    $('.powermail_fieldwrap_type_input input, .powermail_fieldwrap_type_textarea textarea, .form-input:not(.form-input-checkbox) input, .form-input textarea, .input-container input, .input-container textarea').focusout(function() {
      if ($(this).val() === "") {
          $(this).parent().removeClass('complete');
      }
      $(this).parent().removeClass('active');
    });
}

//CUSTOM SELECT
function custom_select() {
    $('.powermail_select, .form-input select').each(function(i, select) {
        if (!$(this).next().hasClass('dropdown')) {
            $(this).after('<div class="dropdown ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
            var dropdown = $(this).next();
            var options = $(select).find('option');
            var selected = $(this).find('option:selected');
            dropdown.find('.current').html(selected.data('display-text') || selected.text());
            options.each(function(j, o) {
                var display = $(o).data('display-text') || '';
                dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + $(o).text() + '</li>');
            });
        }
    });

    $('.dropdown.powermail_select').each(function(index) {
        $(this).attr('data-nth', index + 1);
    });
}

//CUSTOM SELECT - Open/close
$(document).on('click', '.dropdown', function(event) {
    $('.dropdown').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).find('.option').attr('tabindex', 0);
        $(this).find('.selected').focus();
    } else {
        $(this).find('.option').removeAttr('tabindex');
        $(this).focus();
    }
});
//CUSTOM SELECT - Close when clicking outside
$(document).on({
    'touchstart': function(event) {
        if ($(event.target).closest('.dropdown').length === 0) {
            $('.dropdown').removeClass('open');
            $('.dropdown .option').removeAttr('tabindex');
        }
        event.stopPropagation();
    }
});

$(document).click(function(event) {
    if ($(event.target).closest('.dropdown').length === 0) {
        $('.dropdown').removeClass('open');
        $('.dropdown .option').removeAttr('tabindex');
    }
    event.stopPropagation();
});

//CUSTOM SELECT - Option click
$(document).on('click', '.dropdown .option', function(event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown').find('.current').text(text);
    $(this).closest('.dropdown').prev('select').val($(this).data('value')).trigger('change');
});

// Menu sticky
function menuSticky() {
    var scroll = $(window).scrollTop();
    var $menu = $("#header");
    // Do something
    if (scroll > 50) {
        if (!$menu.hasClass("sticky")) {
            $menu.addClass("sticky");
        }
    } else {
        if ($menu.hasClass("sticky")) {
            $menu.removeClass("sticky");
        }
    }
    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        var $menu = $("#header");
        // Do something
        if (scroll > 50) {
            if (!$menu.hasClass("sticky")) {
                $menu.addClass("sticky");
            }
        } else {
            if ($menu.hasClass("sticky")) {
                $menu.removeClass("sticky");
            }
        }
    });
}

//HEADER SUBMENU ANCHOR
function submenuAnchor() {
    $('.anchor-menu p + ul > li > a[href*="#"]').on('click', function (e) {
      var hrefTarget = $(this).attr('href').replace(/.*(#.*)$/, "$1");
      $('html, body').animate({
        scrollTop: ($(hrefTarget).offset().top - 175)
      }, 500);
      e.preventDefault();
    });
  
    $(window).scroll(function() {
        var scrollDistance = $(window).scrollTop();
        $('.anchor-menu li a[href*="#"]').each(function() {
            var hrefTarget = $(this).attr('href').replace(/.*(#.*)$/, "$1");

            if(scrollDistance > $(hrefTarget).offset().top - 176){
                $('.anchor-menu li a[href*="#"]').removeClass('active');
                $('.anchor-menu li a[href*="'+ hrefTarget +'"]').addClass('active');
            }
        });
    });
    var scrollDistance = $(window).scrollTop();
    $('.anchor-menu li a[href*="#"]').each(function() {
        var href = $(this).attr('href');
        var hrefTarget = $(this).attr('href').replace(/.*(#.*)$/, "$1");
        
        if(scrollDistance > $(hrefTarget).offset().top - 176){
            $('.anchor-menu li a[href*="#"]').removeClass('active');
            $('.anchor-menu li a[href*="'+ hrefTarget +'"]').addClass('active');
        }
    });
}

 //Gallery
 function lightgallery() {
    if ($(".lightgallery").length) {
        $(".lightgallery").lightGallery({
            selector: ".video-box",
            hash: false,
        });
    }
}

//Slider gallery
function sliderGallery() {
    $('.slider-gallery').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        swipe: false,
        draggable: false,
        dots: true,
    });
}

//Menu open	
function menuOpen() {
    $('.menu-burger').click(function () {
        $(this).stop().toggleClass('open');
        $('.nav-menu').stop().toggleClass('open');
        $('body, html').stop().toggleClass('no-scroll');
    });
}

//SUBMENU ON MOBILE
function submenuMobile() {
    $('.navbar-nav li.hassub > a').on('click', function(e) {
        e.preventDefault();
        var linkTitle = $(this).html();
        $(this).parents('.navbar-nav').addClass('open-sub');
        $('.header-menu').addClass('open-sub');
        $(this).siblings('.dropdown-menu').addClass('open-sub');
        if (!$('.dropdown-menu > .submenu-title').length) {
            $(this).siblings('.dropdown-menu').prepend('<p class="submenu-title"><i class="icon2-arrow-button-left-17"></i>' + linkTitle + '</p>');
        }
    });

    $(document).on('click', '.dropdown-menu > .submenu-title', function() {
        $(this).parent('.dropdown-menu').removeClass('open-sub');
        $(this).parents('.hasSub').removeClass('open');
        $(this).parents('.navbar-nav').removeClass('open-sub');
        $('.header-menu').removeClass('open-sub');
        $(this).remove();
    });
}

$(document).ready(function () {
    var windowWidth = $(window).outerWidth();

    menuSticky();
    menuOpen();
    if (windowWidth < 992) {
        submenuMobile();
    }
    accordeon();
    accordeonOpenFromURL();
    carouselTab();
    formsAnimation();
    custom_select();
    submenuAnchor();
    lightgallery();
    sliderGallery();
});

$(window).resize(function() {
    var windowWidth = $(window).outerWidth();

    if (windowWidth < 992) {
        submenuMobile();
    }
});