# Agenda Embed

Web component to browse, filter and search events; including a detail view.

## Installation

### A. By downloading

You can [download the latest version](https://gitlab.com/geocity/agenda-embed/-/releases) on Gitlab to serve it from your own server.

### B. Using a third-party CDN

You can directly hotlink the JavaScript file mentioned below through a CDN like Unpkg or JSDelivr:

```html
<script type="module" src="https://unpkg.com/@geocity/agenda-embed"></script>

<script
  type="module"
  src="https://cdn.jsdelivr.net/npm/@geocity/agenda-embed"
></script>
```

### C. Using a package manager

The widget is available as a package that you can install using your favorite Node package manager; example using npm:

```bash
npm install @geocity/agenda-embed
```

To define the custom `agenda-embed` element, you must import the package somewhere in your code:

```js
import '@geocity/agenda-embed';
```

## Usage

Embed the web component directly in an HTML page:

```html
<!-- Customize the style -->
<style>
  agenda-embed {
    --agd-color-primary: #008c6f;
    --agd-color-accent: #005d95;
    --agd-headings-font-family: 'Playfair Display', serif;
    --agd-headings-font-weight: 500;
    /* ... */
  }
</style>

<!-- Render the component -->
<agenda-embed></agenda-embed>

<!-- Initialize the component -->
<script type="module" src="/path/to/agenda-embed.js"></script>
```

### Filter events displayed by domain

You can globally restrict the widget to events of a given domain by using the `domain` attribute on the component:

```html
<agenda-embed domain="sport"></agenda-embed>
```

A domain must be specified for the filters dropdown to be displayed.

To display events from all domains, don’t specify this attribute.

### Customize the API target

Specify the `api-base-url` attribute to use a different API target:

```html
<agenda-embed api-base-url="https://www.yourdomain.ch/api"></agenda-embed>
```

This parameter is optional, `https://geocity.ch/rest` is the default.

The end-points called by the widget are:

- GET `/agenda` for the list of events
- GET `/agenda/:id` for the event details

### Scroll position and sticky headers

When navigating between the list view and the detail view, the scroll position will be adjusted. For example:

- When going from the list to the detail, it will move the scroll position to the top of the `agenda-embed` element.
- When going back to the list, it will restore the scroll position where it was before.

If the website you are embedding the widget in has a sticky element (like a header) at the top of the page, it might cover the top of the widget content. The `scroll-offsets` attribute gives the widget insights about this element to improve the scroll position:

```html
<agenda-embed scroll-offsets="[[0, 80], [992, 150]]"></agenda-embed>
```

The value must be a valid JSON where the key is the viewport min-width and the value the offset to be applied. The example above can be described as:

- Starting from a viewport width of `0` pixels, offset the scroll position by `80` pixels.
- Starting from a viewport width of `992` pixels, offset the scroll position by `150` pixels.

You can add as many breakpoints as you need.

## Upcoming events widget

Display the 5 next events in a small widget with just the date and title:

```html
<!-- Customize the style -->
<style>
  agenda-upcoming-embed {
    --agd-color-primary: #008c6f;
    /* ... */
  }
</style>

<!-- Render the component -->
<agenda-upcoming-embed></agenda-upcoming-embed>

<!-- Initialize the component -->
<script type="module" src="/path/to/agenda-upcoming-embed.js"></script>
```

The options `domain` and `api-base-url` works as well on this component.

### Link to the event details

To enable links to the events page, the URL of the page hosting the `agenda-embed` widget must be provided in the `agenda-url` option.

```html
<agenda-upcoming-embed agenda-url="/medias/agenda"></agenda-upcoming-embed>
```
