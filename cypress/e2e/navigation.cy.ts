describe('Navigation', () => {
  describe('scroll position', () => {
    it('is not altered on page load', () => {
      cy.visit('/yverdon.html#/');

      cy.contains('h2', 'Castrum Festival');

      cy.document().then((document) => {
        expect(document.documentElement.scrollTop).to.equal(0);
      });
    });

    it('is moved to the top of the agenda when going forward, without scroll offset', () => {
      cy.visit('/#/');

      cy.contains('h2', 'Blaise Bersinger').scrollIntoView();

      cy.document().then((document) => {
        expect(document.documentElement.scrollTop).to.be.at.least(1714);
      });

      cy.contains('h2', 'Blaise Bersinger').click();

      cy.document().then((document) => {
        expect(document.documentElement.scrollTop).to.equal(0);
      });
    });

    it(
      'is moved to the top of the agenda when going forward, with small scroll offset',
      { viewportWidth: 400 },
      () => {
        cy.visit('/yverdon.html#/');

        cy.contains('h2', 'Castrum Festival').click();

        cy.document().then((document) => {
          expect(document.documentElement.scrollTop).to.equal(153);
        });
      }
    );

    it('is moved to the top of the agenda when going forward, with large scroll offset', () => {
      cy.visit('/yverdon.html#/');

      cy.contains('h2', 'Castrum Festival').click();

      cy.document().then((document) => {
        expect(document.documentElement.scrollTop).to.equal(232);
      });
    });

    it('is restored when going back', () => {
      cy.visit('/yverdon.html#/');

      cy.contains('h2', 'Castrum Festival');

      cy.get('a[href="#/event/1"]').scrollIntoView().click();

      cy.url().should('contain', '#/event/1');

      cy.go('back');

      cy.document().then((document) => {
        expect(document.documentElement.scrollTop).to.equal(513);
      });
    });
  });
});
