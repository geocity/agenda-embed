describe('Upcoming', () => {
  it('displays a loader and then the 5 latest events with date and title', () => {
    cy.visit('/upcoming.html');

    cy.get('agenda-upcoming-embed').within(() => {
      cy.contains('Chargement en cours…');

      cy.get('li').should('have.length', 5);

      cy.contains('li', '24 août 2023')
        .find('a')
        .should('have.attr', 'href', '/index.html#/event/1')
        .and('contain', 'Castrum Festival');
      cy.contains('li', 'Du 30 au 31 août 2023')
        .find('a')
        .should('have.attr', 'href', '/index.html#/event/2')
        .and('contain', 'La dérivée');
      cy.contains('li', '3 sept. 2023')
        .find('a')
        .should('have.attr', 'href', '/index.html#/event/3')
        .and('contain', 'Actyv Été');
      cy.contains('li', 'Du 10 au 11 sept. 2023')
        .find('a')
        .should('have.attr', 'href', '/index.html#/event/4')
        .and('contain', 'Triathlon d’Yverdon-les-Bains');
      cy.contains('li', 'Du 27 sept. au 10 oct. 2023')
        .find('a')
        .should('have.attr', 'href', '/index.html#/event/5')
        .and('contain', 'Tour de Romandie Féminin');
    });
  });
});
