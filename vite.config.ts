import vue from '@vitejs/plugin-vue';
import { transform } from 'esbuild';
import kebabCase from 'lodash/kebabCase';
import { resolve } from 'node:path';
import { URL, fileURLToPath } from 'node:url';
import { type InputOption } from 'rollup';
import { Plugin, UserConfig, defineConfig, loadEnv } from 'vite';

function minifyBrowserBundles() {
  return {
    name: 'minifyBrowserBundles',
    renderChunk: {
      order: 'post',
      async handler(code) {
        return await transform(code, {
          minify: true,
          // Ensure dev-only code is properly tree-shaked
          define: {
            'process.env.NODE_ENV': '"production"',
          },
        });
      },
    },
  } as Plugin;
}

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '');
  // When build for production, do one entry at a time, to avoid creating chunks
  // with the code shared between entry points
  let entry: InputOption = resolve(
    __dirname,
    'src',
    env.ENTRY ?? 'agenda-embed.ts'
  );

  // In development, serve all entry points together
  if (command === 'serve') {
    entry = [
      resolve(__dirname, 'src/agenda-embed.ts'),
      resolve(__dirname, 'src/agenda-upcoming-embed.ts'),
    ];
  }

  const config: UserConfig = {
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      },
    },

    build: {
      emptyOutDir: !env.ENTRY,
      copyPublicDir: false,
      lib: {
        entry,
        fileName: (format, entryName) => {
          return `${kebabCase(entryName)}.js`;
        },
        formats: ['es'],
      },
    },

    plugins: [
      vue({
        template: {
          compilerOptions: {
            whitespace: 'preserve',
          },
        },
      }),
    ],

    server: {
      port: 3000,
    },
  };

  config.build ??= {};
  config.build.rollupOptions ??= {};
  config.plugins ??= [];

  if (command === 'serve') {
    config.build.rollupOptions.input = {
      main: resolve(__dirname, 'index.html'),
      yverdon: resolve(__dirname, 'yverdon.html'),
    };
  } else {
    config.plugins.push(minifyBrowserBundles());
  }

  return config;
});
